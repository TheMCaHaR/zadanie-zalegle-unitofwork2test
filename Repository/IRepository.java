package Repository;

import main.User;

public interface IRepository {
	void add(Entity entity);
	void delete(Entity entity);
	void modify(Entity entity);
	Entity getEntityById(int id);
}
