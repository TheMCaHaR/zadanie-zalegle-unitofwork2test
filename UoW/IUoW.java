package UoW;

import Repository.Entity;
import Repository.IRepository;

public interface IUoW {
    void saveChanges();
    void undo();

    void markAsNew(Entity entity, IRepository repository);
    void markAsDeleted(Entity entity, IRepository repository);
    void markAsChanged(Entity entity, IRepository repository);
}